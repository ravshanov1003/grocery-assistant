"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor
# from termcolor import colored

def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if(n<=-1):
        print("Столько нет")
    elif(n>=2 and n<=4):
        print("Пожалуйста,", n, "яблока")
    elif(n==1 or n==21):
        print("Пожалуйста,", n, "яблоко")
    elif(n>=22 and n<=24):
        print("Пожалуйста,", n, "яблока")
    elif(n>1 and n<=30):
        print("Пожалуйста,", n, "яблок")
    elif(n>=31):
        print("Столько нет")


if __name__ == "__main__":
    termcolor.cprint("Grocery assistant", 'blue')  # color this caption
    main()
